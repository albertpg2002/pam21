# LDAP Server
## Rubén Rodríguez ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### PAM Containers:

* **rubeeenrg/pam21:base** container de treball de PAM. Organització: edt.org

```
docker build -t rubeeenrg/pam21:base
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisix --privileged -it rubeeenrg/pam21:base /bin/bash
bash startup
```

*Pràctica 07:*
/etc/pam.d/chfn

```
account optional pam_echo.so [type:account rhost: %H lhost; %h service: %s terminal: %t ruser: %U user: %u]
account optional pam_echo.so [entra al time]
account sufficient pam_time.so debug
account optional pam_echo.so [surt del time]
account required pam_deny.so
```

/etc/security/time.conf

```
1
chfn;*;*;Al0800-1000;1600-1800

2
chfn;*;*;!Al0800-1000;!1600-1800

3
chfn;*;unix01;Al0800-1400

4
chfn;*;unix01;Wk0000-2400

5:
chfn;*;unix02;Al1300-0800
```

*Pràctica 08:*
/etc/security/pam_mount.conf.xml

```
<volume
       user="*"
       fstype="tmpfs"
       mountpoint="~/mytmp"
       options="size=100M" 
     />
```
```
<volume
       user="unix01"
       fstype="tmpfs"
       mountpoint="~/tmp"
       options="size="200M" 
     />
```
```
DINS DEL HOST:
El nfs s'ha d'instalar i executar al nostre host
apt install nfs-server
systemctl nfs-server-start

vim /etc/exports, afegim:
     /usr/share/doc *(ro,sync)

DINS DEL DOCKER:
service nfs-common start (si no va...: service rpcbind restart)
vim /etc/security/pam_mount.conf.xml
service rpcbind restart
service nfs-common start
PER PROBAR:
su unix02
df
<volume
       user="unix02"
       fstype="nfs4"
       server="172.18.0.2"
       path="/usr/share/doc"
       mountpoint="~/mydocs" 
     />
```
