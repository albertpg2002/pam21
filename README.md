# PAM - Rubén Rodríguez García M06-ASO
# Curs 2021-2022

* **rubeeenrg/pam21:base** Container PAM base per practicar regles PAM. 
   
   Utilitza chfn per practicar els exmples del HowTo per aprendre el funcionament dels *type* (*auth*, *account*,*session* i *password*) i 
   dels *control* bàsics (*sufficient*, *required*, *requisite* i *optional*) i avançats (*die*, *ok*). 

   També permet practicar *pam_mount.so* per muntar unitats de *tmpfs* o *NFS* als usuaris.

* **rubeeenrg/pam21:ldap** Container PAM per practicar l'autenticació PAM unix i PAM ldap. 

  Utilitza els paquets *libpam-ldap*, *libnss-ldap*, *nscd* i *nslcd* per configurar l'accés al servei ldap i configura les regles PAM 
  per permetre tant usuaris unix com usuaris LDAP. En tots dos casos es munta en el home un recurs *tmpfs* temporal. En el cs dels usuaris LDAP si el seu home no existeix es crea usant *pam_mkhomedir.so*.

* **rubeeenrg/pam21:python** Container host pam basat en *pam21:base* per practicar crear una aplicació PAM Aware i per crear el nostre propi mòdul de PAM. 

  Amb l'aplicació PAM Aware *pamware.py* es fa un program a que mostra els números del 1 al 10 però sempre i quant l'usuari que l'executa sigui un usuari autenticat (*pam_unix.so*). 
  
  Es dissenya un mòdul propi de PAM anomenat *pam_mates.py* que autentica els usuaris segons si saben respondre o no a una pregunta de mates. Els usuaris que en saben queden autenticats, si no diuen la resposta 
  correcta es denega l'autenticació. Per poder usar un modul pam escrit en python cal descarregar, compilar i incorporar com a llibreia el mòdul *pam_pyhton.so*.
